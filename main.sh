#!/bin/bash
# shellcheck source=/dev/null
set -u

readonly scripts=('vim_plugins_install.sh' 'text.sh')
for script in "${scripts[@]}"
do
	# shellcheck source=/dev/null
	if [ -f "${script}" ]; then
		source "${script}"
	else
		echo -f "There is no required ${script} files"
		exit 1;
	fi
done

#The function checks Linux distrubution
function check_Linux_distribution (){
				echo -e "${BLUE}Checking distributions${BLUE}"
				local linux_distribute=("Debian" "Ubuntu" "Kali")
				check_distribute=$(lsb_release -i | awk '{print $3}')
				local count=1
				for distribute  in ${linux_distribute[*]}
				do
					if [ "$check_distribute" == "$distribute" ];then
						echo "${GREEN}ok${GREEN}"
						break
					elif [ "${#linux_distribute[*]}" -eq "$count" ]; then 
						echo -e "${RED}Your distribution is not listed${RED}"
						exit 1
					fi
					count=$(( count + 1  ))
				done
}

#the function checks for system update 
function check_system_update (){
	echo -e "${BLUE}The system is being updated, please wait ............${WHITE}"
	sudo apt update &> /dev/null 
}

#The function checks internet connect
function check_internet_connect (){
	if ! ping -c 2 www.google.pl &> /dev/null; then
		echo -e "${RED}there is no internet connection${WHITE}"; exit 2
	else 	
		echo -e "${GREEN}there is an internet connection${WHITE}"
	fi
}

#This function installs plugins for vim   
function install_vim_plugins (){
	if  dpkg-query -l 'vim' &> /dev/null; then
		installation		
	else
		echo -e "${RED}The program can't install plugins because  vim is not installed ${WHITE}"
	fi

}

#The function shows program version 
function version_program (){
	case  $1 in  
		"-v")
			ver=2.2
			echo "The program version  $ver"
			;;
	esac
}


: '
The status_programs function checks whether the programs have been installed
If the Program is not installed. This  program will be installed
'
function status_programs (){
		echo -e "\n${BLUE}Checking the status of programs"
		if sudo dpkg-query -s  "vim" &> /dev/null; then
			echo -e "${GREEN}vim - installed" 
		else
			echo -e "${RED}vim - not installed"; sudo apt install -y vim &> /dev/null
		fi
		if sudo dpkg-query -s "curl" &> /dev/null; then
			echo -e "${GREEN}curl - installed" 
		else
			echo -e "${RED}curl - not installed"; sudo apt install -y curl &> /dev/null
		fi
}

#The main function call all functions
function main (){
	version_program "$1"
	check_Linux_distribution
	check_internet_connect
	check_system_update
	status_programs
	install_vim_plugins
}
main "$1"
