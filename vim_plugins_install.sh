#!/bin/bash
# shellcheck source=/dev/null
if [ -f text.sh ]; then
	source text.sh
else
	echo -f "${RED}There is no required text.sh file${WHITE}"
	exit 1;
fi

#The function installs plugins 
function plugins_installation (){
	sciezka_skryptu="$PWD"
    declare -rA plugins=(["pathogen.vim"]="https://tpo.pe/pathogen.vim" ["plug.vim"]="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" ["auto-pairs"]="https://github.com/jiangmiao/auto-pairs" 
    ["nerdtree"]="https://github.com/scrooloose/nerdtree" ["supertab"]="https://github.com/ervandew/supertab.git" ["syntastic"]="https://github.com/scrooloose/syntastic"
	["ansible-vim"]="https://github.com/pearofducks/ansible-vim" ["vim-go"]="https://github.com/fatih/vim-go.git")	
	mapfile -t users < <(cat < '/etc/passwd' | sed -e  's/:/ /g' |  awk '{ if ($3 >= 1000 && $3 < 9999) printf("%s\n", $1) }')
	for user in "${users[@]}";do
		Directory_Autoload="/home/$user/.vim/autoload"
		Directory_Bundle="/home/$user/.vim/bundle"
		if [[ ! -d $Directory_Autoload &&  ! -d  $Directory_Bundle ]];then
				mkdir -p  "$Directory_Autoload" "$Directory_Bundle"  
				echo "Create directory $Directory_Autoload and $Directory_Bundle"
		fi					
 		for plugin in "${!plugins[@]}"; do
            if [ "$plugin" = "pathogen.vim"  ] || [ "$plugin" = "plug.vim" ]; then
				cd  "$Directory_Autoload" || return 
            	if [ ! -f "${Directory_Autoload}/${plugin}" ];then
                    curl -LSso "${Directory_Autoload}/${plugin}" "${plugins[$plugin]}" && echo "${GREEN}$plugin plugin installed ${WHITE}"
                fi
            else  
			    cd  "$Directory_Bundle" || return 
                if [ ! -d "${Directory_Bundle}/${plugin}" ]; then
 	 	 	        git clone "${plugins[$plugin]}" && echo "${GREEN}$plugin plug installed${WHITE}"
		        fi
            fi
        done 
		mkdir -p ~/.vim/pack/tpope/start
		git clone https://tpope.io/vim/fugitive.git ~/.vim/pack/tpope/start/fugitive
		vim -u NONE -c "helptags ~/.vim/pack/tpope/start/fugitive/doc" -c q
		cd "$sciezka_skryptu" || return	
		sudo cp -r config "/home/$user/.vim/vimrc"
		echo "${GREEN}The config file has been copied${WHITE}"
		sudo chown -R "$user:$user" "/home/$user/.vim/"			
	done
}

#The function  
function installation (){
 		 read -r -p "${MAGENTA}Do you want to install the basic plugins for vim [y/n]${WHITE} " decyzja
		 if [ "$decyzja" == 'y'  ]; then
 				echo -e "${YELLOW}plugins are being installed............."
				plugins_installation
		 elif [ "$decyzja" == 'n' ]; then	
				echo -e "${CYAN}not installed plugins for vim ${WHITE}"
		 else 	
				echo -e "${RED}there is no such option in the program ${WHITE}"
		 fi 
}