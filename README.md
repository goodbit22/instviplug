# instviplug

The program is used to install plugins for vim such as

* pathogen
* auto-pairs  
* nerdtree
* supertab
* syntastic
* vim-plugins
* vim-go
* ansible-vim
* fugitive
  
and their configuration  

## Technology

* bash
* awk

## Running

```bash
    sudo bash main.sh
```
